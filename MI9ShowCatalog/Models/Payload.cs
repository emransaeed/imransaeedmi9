﻿using System;

namespace MI9ShowCatalog.Models
{
    public class Payload
    {
        public string slug { get; set; }
        public string title { get; set; }
        public string tvChannel { get; set; }
        public string country { get; set; }
        public string description { get; set; }
        public Boolean? drm { get; set; }
        public int? episodeCount { get; set; }
        public string genre { get; set; }
        public string language { get; set; }
        public string primaryColour { get; set; }
        public Image image { get; set; }
        public Season[] seasons { get; set; }
        public NextEpisode nextEpisode { get; set; }
    }
}