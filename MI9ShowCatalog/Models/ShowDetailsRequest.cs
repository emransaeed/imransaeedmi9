﻿namespace MI9ShowCatalog.Models
{
    public class ShowDetailsRequest
    {
        public int skip { get; set; }
        public int take { get; set; }
        public int totalRecords { get; set; }
        public Payload[] payload { get; set; }
    }
}