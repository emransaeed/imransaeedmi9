﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MI9ShowCatalog.Models
{
    public class ShowDetailsResponse
    {
        public string slug { get; set; }
        public string title { get; set; }
        public string image { get; set; }
    }
}