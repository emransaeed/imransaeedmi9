﻿using System.Linq;
using MI9ShowCatalog.Models;

namespace MI9ShowCatalog.Services
{
    public class ShowSelectorService : IShowSelectorService
    {
        public ShowDetailsResponse[] DrmAndEpisodeCountSelection(ShowDetailsRequest request)
        {
            var response = from show in request.payload.Where(p => p.drm != null && p.drm == true &&
                           p.episodeCount != null && p.episodeCount > 0)
                           select new ShowDetailsResponse()
                           {
                               image = show.image != null ? show.image.showImage : null,
                               slug = show.slug,
                               title = show.title
                           };

            return response.ToArray();
        }
    }
}