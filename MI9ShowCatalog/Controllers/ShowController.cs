﻿using System.Web.Http;
using MI9ShowCatalog.Models;
using MI9ShowCatalog.Services;

namespace MI9ShowCatalog.Controllers
{
    public class ShowController : ApiController
    {
        private readonly IShowSelectorService _showSelectorService;

        public ShowController()
        {
            _showSelectorService = new ShowSelectorService();
        }

        [HttpPost]
        public IHttpActionResult GetPopularShows([FromBody] ShowDetailsRequest request)
        {
            ValidateRequest(request);

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            return Ok(_showSelectorService.DrmAndEpisodeCountSelection(request));
        }

        private void ValidateRequest(ShowDetailsRequest request)
        {
            if (request == null)
            {
                ModelState.AddModelError("error", "Could not decode request: JSON parsing failed");
            }
        }
    }
}
