﻿using System.Web.Http;

namespace MI9ShowCatalog.App_Start
{
    public class WebApiConfig
    {
        public static void Configure(HttpConfiguration config)
        {
            config.MapHttpAttributeRoutes();

            config.Routes.IgnoreRoute("", "{resource}.axd/{*pathInfo}");
            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "{controller}/{action}",
                defaults: new
                {
                    controller = "Show",
                    action = "GetPopularShows"
                }
            );
        }
    }
}