﻿using MI9ShowCatalog.Models;

namespace MI9ShowCatalog.Services
{
    public interface IShowSelectorService
    {
        ShowDetailsResponse[] DrmAndEpisodeCountSelection(ShowDetailsRequest request);
    }
}
